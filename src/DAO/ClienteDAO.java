package DAO;

import java.util.Date;
import javax.swing.JOptionPane;
import objeto.Cliente;

public class ClienteDAO {
    
    public void gravar(String nome, String CPF, String RG, Date dataNascimento, String logradouro, String numero, String complemento, String cep, String bairro, String cidade, String estado, String telefone, String email ) {
        
        Cliente cliente = new Cliente();
        cliente.setNome(nome);
        cliente.setCPF(CPF);
        cliente.setRG(RG);
        cliente.setDataNascimento(dataNascimento);
        cliente.setLogradouro(logradouro);
        cliente.setNumero(Integer.parseInt(numero));
        cliente.setComplemento(complemento);
        cliente.setCEP(cep);
        cliente.setBairro(bairro);
        cliente.setCidade(cidade);
        cliente.setEstado(estado);
        cliente.setTelefone(telefone);
        cliente.setEmail(email);
      
        Banco b = Banco.getInstance();
        int posicao = b.getIndiceClientes();
        b.getClientes().put(b.getIndiceClientes(), cliente);
        b.addIndiceCliente();
        b.gravarArquivo();
    }
    
    public void alterar(int coluna, Integer indice, Object valor) {
        Cliente cliente = Banco.getInstance().getClientes().get(indice);
        switch (coluna) {
            case 1:
                cliente.setNome((String) valor);
                break;
            case 2:
                cliente.setCPF((String) valor);
                break;
            case 3:
                cliente.setRG((String) valor);
                break;
            case 4:
                cliente.setDataNascimento((Date) valor);
                break;
            case 5:
                cliente.setLogradouro((String) valor);
                break;
            case 6:
                cliente.setNumero(Integer.parseInt((String) valor));
                break;
            case 7:
                cliente.setComplemento((String) valor);
                break;
            case 8:
                cliente.setCEP((String) valor);
                break;
            case 9:
                cliente.setBairro((String) valor);
                break;
            case 10:
                cliente.setCidade((String) valor);
                break;
            case 11:
                cliente.setEstado((String) valor);
                break;
            case 12:
                cliente.setTelefone((String) valor);
                break;
            case 13:
                cliente.setEmail((String) valor);
                break;
        }
        Banco.getInstance().gravarArquivo();
    }
    
    public void alterar_cadastro(Integer indice, String nome, String CPF, String RG, Date dataNascimento, String logradouro, int numero, String complemento, String cep, String bairro, String cidade, String estado, String telefone, String email) {
        Cliente cliente = Banco.getInstance().getClientes().get(indice);
        cliente.setNome(nome);
        cliente.setCPF(CPF);
        cliente.setRG(RG);
        cliente.setDataNascimento((Date) dataNascimento);
        cliente.setLogradouro(logradouro);
        cliente.setNumero(numero);
        cliente.setComplemento(complemento);
        cliente.setCEP(cep);
        cliente.setBairro(bairro);
        cliente.setCidade(cidade);
        cliente.setEstado(estado);
        cliente.setTelefone(telefone);
        cliente.setEmail(email);
        Banco.getInstance().gravarArquivo();
    }
    
    
    
    
    public Cliente buscar(Integer indice) {
        return Banco.getInstance().getClientes().get(indice);
    }
    
    public void apagar(Integer indice) {
        // perder mais tempo nisso depois
        Banco.getInstance().getClientes().remove(indice);
        Banco.getInstance().gravarArquivo();
    }
}
