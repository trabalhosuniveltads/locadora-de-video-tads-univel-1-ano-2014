package DAO;

import javax.swing.JOptionPane;
import objeto.Cliente;
import objeto.Filme;

public class FilmeDAO {
    
    public void gravar(String titulo, int anoLancamento, int qtde, String censura, String genero, boolean lancamento) {
        
        Filme filme = new Filme();
        filme.setTitulo(titulo);
        filme.setAnoLancamento(anoLancamento);
        filme.setQuantidade(qtde);
        filme.setCensura(censura);
        filme.setGenero(genero);
        filme.setLancamento(lancamento);

        Banco b = Banco.getInstance();
        int posicao = b.getIndiceFilmes();
        b.getFilmes().put(b.getIndiceFilmes(), filme);
        b.addIndiceFilmes();
        b.gravarArquivo();
    }
    
    public void alterar(int coluna, Integer indice, Object valor) {
        Filme filme = Banco.getInstance().getFilmes().get(indice);
        switch (coluna) {
            case 1:
                filme.setTitulo((String) valor);
                break;
            case 2:
                filme.setAnoLancamento((Integer) valor);
                break;
            case 3:
                filme.setQuantidade((Integer) valor);
                break;
            case 4:
                filme.setCensura((String) valor);
                break;
            case 5:
                filme.setGenero((String) valor);
                break;
            case 6:
                filme.setLancamento((Boolean) valor);
                break;
        }
        Banco.getInstance().gravarArquivo();
    }
    
    public void alterar_cadastro(int indice, String titulo, int anoLancamento, int qtde, String censura, String genero, boolean lancamento) {
        Filme filme = Banco.getInstance().getFilmes().get(indice);
      
        filme.setTitulo(titulo);
        filme.setAnoLancamento(anoLancamento);
        filme.setQuantidade(qtde);
        filme.setCensura(censura);
        filme.setGenero(genero);
        filme.setLancamento(lancamento);
        
        Banco.getInstance().gravarArquivo();
    }
    
    
    public Filme buscar(Integer indice) {
        return Banco.getInstance().getFilmes().get(indice);
    }
    
    public void apagar(Integer indice) {
        // perder mais tempo nisso depois
        Banco.getInstance().getFilmes().remove(indice);
        Banco.getInstance().gravarArquivo();
    }
}
