package DAO;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import objeto.Cliente;
import objeto.Filme;
import objeto.Locacao;
import objeto.LocacaoItem;

public class LocacaoDAO {
    
    public void gravar(Cliente cliente, HashMap<Integer, LocacaoItem> itens, Date dataLocacao, 
            String situacao, float valorTotal, float valorTotalPago, int qtdItens) {
        
        Locacao locacao = new Locacao();
        locacao.setCliente(cliente);
        locacao.setItens(itens);
        locacao.setDataLocacao(dataLocacao);
        locacao.setSituacao(situacao);
        locacao.setValorTotal(valorTotal);
        locacao.setValorTotalPago(valorTotalPago);
        locacao.setQtdItens(qtdItens);
        Banco b = Banco.getInstance();
        int posicao = b.getIndiceLocacoes();
        b.getLocacoes().put(b.getIndiceLocacoes(), locacao);
        b.addIndiceLocacoes();
        b.gravarArquivo();
        //JOptionPane.showMessageDialog(null, "Gravado com sucesso!");
        b.getMapLocacoes().clear();
        b.resetIndiceMapLocacoes();
    }
    
    
    public void alterar_cadastro(int indice, Cliente cliente, HashMap<Integer, LocacaoItem> itens, Date dataLocacao,
            String situacao, float valorTotal, float valorTotalPago, int qtdItens) {
        
        Locacao locacao = Banco.getInstance().getLocacoes().get(indice);
        
        locacao.setCliente(cliente);
        locacao.setItens(itens);
        locacao.setDataLocacao(dataLocacao);
        locacao.setSituacao(situacao);
        locacao.setValorTotal(valorTotal);
        locacao.setValorTotalPago(valorTotalPago);
        locacao.setQtdItens(qtdItens);

        Banco.getInstance().gravarArquivo();
    }
    
    public Locacao buscar(Integer indice) {
        return Banco.getInstance().getLocacoes().get(indice);
    }
    
    public void apagar(Integer indice) {
        // perder mais tempo nisso depois
        Banco.getInstance().getLocacoes().remove(indice);
        Banco.getInstance().gravarArquivo();
    }
}
