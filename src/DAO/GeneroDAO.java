package DAO;

import javax.swing.JOptionPane;
import objeto.Cliente;

public class GeneroDAO {
    
    public void gravar(String nome) {
        Banco b = Banco.getInstance();
        int posicao = b.getIndiceGenero();
        b.getGeneros().put(b.getIndiceGenero(), nome);
        b.addIndiceGenero();
        b.gravarArquivo();
        //Funcoes.mensagem("Sucesso!", "Gravado com sucesso!");
    }
    
    public void alterar(Integer chave, Object valor) {
        Banco.getInstance().getGeneros().put(chave, (String)valor);
        Banco.getInstance().gravarArquivo();
    }

    public void apagar(Integer chave) {
        // perder mais tempo nisso depois
        Banco.getInstance().getGeneros().remove(chave);
        Banco.getInstance().gravarArquivo();
    }
    
     public String buscar(Integer indice) {
        return Banco.getInstance().getGeneros().get(indice);
    }
     
}
