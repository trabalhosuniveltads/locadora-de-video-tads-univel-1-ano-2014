package DAO;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.HashMap;
import javax.swing.JOptionPane;
import objeto.Cliente;
import objeto.Filme;
import objeto.Locacao;
import objeto.Locadora;

public class Banco implements Serializable{

    private static Banco banco;
    
    private Locadora locadora = new Locadora();
    
    private Integer indiceClientes = 0; //vai servir como indice
    private HashMap<Integer, Cliente> clientes = new HashMap<>();

    private Integer indiceGenero = 0; //vai servir como indice
    private HashMap<Integer, String> generos = new HashMap<>();

    private Integer indiceFilmes = 0; //vai servir como indice
    private HashMap<Integer, Filme> filmes = new HashMap<>();

    private Integer indiceLocacoes = 0; //vai servir como indice
    private HashMap<Integer, Locacao> locacoes = new HashMap<>();

    private Integer indiceMapLocacoes = 0; //vai servir como indice
    private HashMap<Integer, Filme> mapLocacoes = new HashMap<>(); 

    private Banco() {
    }
 
    public static Banco getInstance() {
        return (banco == null) ? (banco = new Banco()) : banco;
    }

    public void gravarArquivo() {
       try {
          FileOutputStream fos = new FileOutputStream("locadora.banco");
          ObjectOutputStream oos = new ObjectOutputStream(fos);
          oos.writeObject(banco);
          oos.close();
          fos.close();
       } catch (FileNotFoundException ex) {
          JOptionPane.showMessageDialog(null, "OPS... Arquivo de banco de dados não foi encontrado.");
       } catch (IOException ex) {
          JOptionPane.showMessageDialog(null, "Erro na gravação");
       }
    }

    public static void lerArquivo() {
       try {
          FileInputStream fis = new FileInputStream("locadora.banco");
          ObjectInputStream ois = new ObjectInputStream(fis);
          banco = (Banco) ois.readObject();
          ois.close();
          fis.close();
       } catch (FileNotFoundException ex) {
          JOptionPane.showMessageDialog(null, "OPS... Arquivo de banco de dados não foi encontrado.");
       } catch (IOException | ClassNotFoundException ex) {
          JOptionPane.showMessageDialog(null, "Erro na leitura");
       }
    }

    public Locadora getLocadora() {
       return locadora;
    }

    public HashMap<Integer, Cliente> getClientes() {
       return clientes;
    }

    public Integer getIndiceClientes() {
       return indiceClientes;
    }

    public void addIndiceCliente() {
       indiceClientes++;
    }

    public HashMap<Integer, String> getGeneros() {
       return generos;
    }

    public Integer getIndiceGenero() {
       return indiceGenero;
    }

    public void addIndiceGenero() {
       indiceGenero++;
    }

    public HashMap<Integer, Filme> getFilmes() {
       return filmes;
    }

    public Integer getIndiceFilmes() {
    return indiceFilmes;
    }

    public void addIndiceFilmes() {
       indiceFilmes++;
    }

    public HashMap<Integer, Locacao> getLocacoes() {
       return locacoes;
    }

    public Integer getIndiceLocacoes() {
       return indiceLocacoes;
    }

    public void addIndiceLocacoes() {
       indiceLocacoes++;
    }

    public HashMap<Integer, Filme> getMapLocacoes() {
       return mapLocacoes;
    }

    public Integer getIndiceMapLocacoes() {
       return indiceMapLocacoes;
    }

    public void addIndiceMapLocacoes() {
       indiceMapLocacoes++;
    }

    public void resetIndiceMapLocacoes() {
       indiceMapLocacoes = 0;
    } 
}