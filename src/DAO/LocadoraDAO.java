package DAO;

import java.math.BigDecimal;
import javax.swing.JOptionPane;
import modelo.Funcoes;

public class LocadoraDAO {
    
    public void gravar(String nome, String telefone, double valorDiariaFilme, int diasLocacaoFilme,
            double valorDiariaFilmeLancamento, int diasLocacaoFilmeLancamento, double valorMultaDia) {
                
        Banco b = Banco.getInstance();
        b.getLocadora().setNome(nome);
        b.getLocadora().setTelefone(telefone);
        b.getLocadora().setValorDiariaFilme(valorDiariaFilme);
        b.getLocadora().setDiasLocacaoFilme(diasLocacaoFilme);
        b.getLocadora().setValorDiariaFilmeLancamento(valorDiariaFilmeLancamento);
        b.getLocadora().setDiasLocacaoFilmeLancamento(diasLocacaoFilmeLancamento);
        b.getLocadora().setValorMultaDia(valorMultaDia);
        b.gravarArquivo();
        //Funcoes.mensagem("Sucesso!", "Gravado com sucesso!");
    }

    
    
}
