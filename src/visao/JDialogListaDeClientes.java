package visao;

import DAO.Banco;
import DAO.ClienteDAO;
import DAO.LocacaoDAO;
import java.awt.Frame;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import modelo.Funcoes;
import objeto.Cliente;
import objeto.Locacao;
import objeto.LocacaoItem;
import static visao.Locadora.b;
import static visao.Locadora.tableModelClientes;

public class JDialogListaDeClientes extends javax.swing.JDialog {
    

    public JDialogListaDeClientes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tbclientes.setModel(tableModelClientes);
        
        tableModelClientes.fireTableDataChanged();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbclientes = new javax.swing.JTable();
        btn_add_cliente = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtbusca = new javax.swing.JTextField();
        btn_buscar = new javax.swing.JButton();
        btn_limpar = new javax.swing.JButton();
        btn_editar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btn_editar1 = new javax.swing.JButton();
        btn_add_venda = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);

        jLabel2.setFont(new java.awt.Font("Tekton Pro Cond", 1, 48)); // NOI18N
        jLabel2.setText("Clientes");

        tbclientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Ativo", "Nome", "CPF"
            }
        ));
        tbclientes.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tbclientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbclientesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbclientes);

        btn_add_cliente.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_add_cliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icon-adicionar.png"))); // NOI18N
        btn_add_cliente.setText("Novo Cliente");
        btn_add_cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_add_clienteActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel5.setText("Buscar");

        txtbusca.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        btn_buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_find.png"))); // NOI18N
        btn_buscar.setText("Buscar");
        btn_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarActionPerformed(evt);
            }
        });

        btn_limpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_refresh.png"))); // NOI18N
        btn_limpar.setText("Limpar");
        btn_limpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_limparActionPerformed(evt);
            }
        });

        btn_editar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_matricula.png"))); // NOI18N
        btn_editar.setText("EDITAR O CLIENTE SELECIONADO");
        btn_editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editarActionPerformed(evt);
            }
        });

        jLabel1.setText("* por motivo de integridade dos dados não será permitido apagar qualquer cliente.");

        btn_editar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_matricula.png"))); // NOI18N
        btn_editar1.setText("Relatório de Locações dos últimos 6 meses");
        btn_editar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editar1ActionPerformed(evt);
            }
        });

        btn_add_venda.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_add_venda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icon-adicionar.png"))); // NOI18N
        btn_add_venda.setText("Nova Locação para o cliente");
        btn_add_venda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_add_vendaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_add_cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 268, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btn_editar)
                                .addGap(18, 18, 18)
                                .addComponent(btn_add_venda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addComponent(btn_editar1))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(txtbusca)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_limpar))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 922, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(btn_add_cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(btn_limpar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn_buscar, javax.swing.GroupLayout.DEFAULT_SIZE, 56, Short.MAX_VALUE)
                            .addComponent(txtbusca))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 373, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(jLabel5)
                        .addGap(403, 403, 403)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_add_venda, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btn_editar, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 2, Short.MAX_VALUE))
                    .addComponent(btn_editar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_add_clienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_add_clienteActionPerformed
        // abrir para adicionar cliente
        JDialogCliente janela = new JDialogCliente((Frame) SwingUtilities.getWindowAncestor(this),true,0,true);
        janela.setLocationRelativeTo(null);
        janela.setVisible(rootPaneCheckingEnabled);
    }//GEN-LAST:event_btn_add_clienteActionPerformed

    public void  localizar(){
        String busca = txtbusca.getText().toLowerCase().trim(); 
        List<Integer> ids = new ArrayList<>();
        
        if (busca.equals("")){
            tableModelClientes.setPesquisa(ids,false);
            //se a busca estiver em branco
            tbclientes.setModel(tableModelClientes);
            tableModelClientes.fireTableDataChanged();
        }else{
            tableModelClientes.setPesquisa(ids,true);
            HashMap<Integer, Cliente> clientes = b.getClientes();
            
            for (Integer key : clientes.keySet()) {
                Cliente cliente = clientes.get(key);
                
                String endereco = cliente.getLogradouro() + " " + Integer.toString(cliente.getNumero()) + " " + cliente.getBairro() + " " +
                                  cliente.getCidade() + cliente.getEstado() + " " + cliente.getCEP();
                
                if((cliente.getNome().toLowerCase().matches("(.*)" + busca + "(.*)"))|
                   (cliente.getEmail().toLowerCase().matches("(.*)" + busca + "(.*)"))|
                   (cliente.getCPF().toLowerCase().matches("(.*)" + busca + "(.*)"))|
                   (cliente.getRG().toLowerCase().matches("(.*)" + busca + "(.*)"))|
                   (endereco.toLowerCase().matches("(.*)" + busca + "(.*)"))|
                   (cliente.getTelefone().toLowerCase().matches("(.*)" + busca + "(.*)"))|
                   (cliente.getEmail().toLowerCase().matches("(.*)" + busca + "(.*)"))
                  ){
                    ids.add(key);              
                }
            }
        }
        tbclientes.setModel(tableModelClientes);
        tableModelClientes.fireTableDataChanged();
    }
    
    private void btn_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarActionPerformed
        // TODO add your handling code here:
        localizar();
    }//GEN-LAST:event_btn_buscarActionPerformed

    private void btn_limparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_limparActionPerformed
       // TODO add your handling code here:
       txtbusca.setText("");
       localizar();
    }//GEN-LAST:event_btn_limparActionPerformed

    private void tbclientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbclientesMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbclientesMouseClicked

    private void btn_editarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editarActionPerformed
        // TODO add your handling code here:
        if(tbclientes.getSelectedRowCount() == 0){
            //mensagem de erro
            JOptionPane.showMessageDialog(rootPane, "Selecione um cliente para editá-lo"); //mostra uma mensagem de erro
        } else {
            int indice = Integer.parseInt(tbclientes.getValueAt(tbclientes.getSelectedRow(), 0).toString());
            JDialogCliente janela = new JDialogCliente((Frame) SwingUtilities.getWindowAncestor(this),true,indice,false);
            janela.setLocationRelativeTo(null);
            janela.setVisible(rootPaneCheckingEnabled);
        }
    }//GEN-LAST:event_btn_editarActionPerformed

    private void btn_editar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editar1ActionPerformed
        // TODO add your handling code here:
        if(tbclientes.getSelectedRowCount() == 0){
            //mensagem de erro
            JOptionPane.showMessageDialog(rootPane, "Selecione um cliente para gerar o relatório"); //mostra uma mensagem de erro
        } else {
            int indice = Integer.parseInt(tbclientes.getValueAt(tbclientes.getSelectedRow(), 0).toString());
            
            //pega o cliente ativo
            ClienteDAO clienteDAO = new ClienteDAO();
            Cliente cliente = (Cliente) clienteDAO.buscar(indice);

            //pega a lista de todas as locacoes
            HashMap<Integer, Locacao> locacoes = b.getLocacoes();
            
            String conteudo_arquivo = "ITEM;NOME;CPF;RG;DATA_NASCIMENTO;LOGRADOURO;NUMERO;COMPLEMENTO;CEP;BAIRRO;CIDADE;ESTADO;TELEFONE;EMAIL;DATA_LOCACAO;SITUACAO;VALOR_LOCACAO;"+
                    "VALOR_PAGO;QTD_ITENS;TITULO;CENSURA;GENERO;ANO;LANCAMENTO;EMPRESTIMO;DATA_DEVOLUCAO;ATRAZO;SITUACAO;ITEM_VALOR_TOTAL;ITEM_VALOR_PAGO\n";
            int contador = 0;    
            for (Integer key : locacoes.keySet()) {
                Locacao locacao = locacoes.get(key); // pega 1 locacao
                if (locacao.getCliente().equals(cliente)){
                    String csv_cliente = "";
                    contador++;
                    //pega a data de agora e adiciona X dias (dependendo do filme)
                    Calendar seis_meses_atras = Calendar.getInstance();
                    seis_meses_atras.setTime(new Date());
                    seis_meses_atras.add(Calendar.DATE, -182);
                    Date de = (Date) seis_meses_atras.getTime();
                        
                    if (Funcoes.compararDatas(locacao.getDataLocacao(), de, new Date(System.currentTimeMillis()))){
                        //Funcoes.mensagem(rootPane, null, "faz até 6 meses " + locacao.getDataLocacao().toString());
                        
                        csv_cliente +=
                        Integer.toString(contador) + ";" +
                        cliente.getNome() + ";" +
                        cliente.getCPF() + ";" +
                        cliente.getRG() + ";" +
                        cliente.getDataNascimento() + ";" +
                        cliente.getLogradouro() + ";" +
                        cliente.getNumero() + ";" +
                        cliente.getComplemento() + ";" +
                        cliente.getCEP() + ";" +
                        cliente.getBairro() + ";" +
                        cliente.getCidade() + ";" +
                        cliente.getEstado() + ";" +
                        cliente.getTelefone() + ";" +
                        cliente.getEmail() + ";" +
                        
                        locacao.getDataLocacao() + ";" +
                        locacao.getSituacao() + ";" +
                        locacao.getValorTotal() + ";" +
                        locacao.getValorTotalPago() + ";" +
                        locacao.getQtdItens()+";";
                        
                        //os itens dessa locacao
                        HashMap<Integer, LocacaoItem> itens = locacao.getItens();
                        for (Integer keyItem : itens.keySet()) {
                            String csv_item = "";
                            LocacaoItem item = itens.get(keyItem); // pega 1 item
                            csv_item +=
                                    item.getFilme().getTitulo() + ";" +
                                    item.getFilme().getCensura() + ";" +
                                    item.getFilme().getGenero() + ";" +
                                    item.getFilme().getAnoLancamento() + ";" +
                                    item.getFilme().isLancamento() + ";" +
                                    item.getDataEmprestimo() + ";" +
                                    item.getDataDevolucao() + ";" +
                                    item.getAtrazo() + ";" +
                                    item.getSituacao() + ";" +
                                    item.getValorTotal() + ";" +
                                    item.getValorTotalPago() +
                                    "\n";
                        conteudo_arquivo += csv_cliente + csv_item;
                        }
                    }
                }
            
            }
            
            
            
            
            //salvar o arquivo
            try {
                String nome_arquivo = "relatorio de locacoes - " + cliente.getNome() + ".csv";
                //construtor que recebe o objeto do tipo arquivo
                FileWriter fw = new FileWriter( nome_arquivo );

                //construtor recebe como argumento o objeto do tipo FileWriter
                BufferedWriter bw = new BufferedWriter( fw );
                //escreve o conteúdo no arquivo
                bw.write( conteudo_arquivo );

                //fecha os recursos
                bw.close();
                fw.close();
                
                Funcoes.mensagem(rootPane, "Sucesso!!", "Sucesso\n Geramos o relatório no arquivo: " + nome_arquivo);

            } catch (IOException ex) {
                Funcoes.mensagem(rootPane, "Erro!", "Problemas ao gerar o arquivo... entre em contato com o suporte técnico.");
            }
            
            
            

            //Funcoes.mensagem(rootPane, null, arquivo);

        }
        
    }//GEN-LAST:event_btn_editar1ActionPerformed

    private void btn_add_vendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_add_vendaActionPerformed
        // TODO add your handling code here:
        if(tbclientes.getSelectedRowCount() == 0){
            //mensagem de erro
            JOptionPane.showMessageDialog(rootPane, "Selecione um cliente para editá-lo"); //mostra uma mensagem de erro
        } else {
            int indice = Integer.parseInt(tbclientes.getValueAt(tbclientes.getSelectedRow(), 0).toString());
            JDialogLocacao janela = new JDialogLocacao((Frame) SwingUtilities.getWindowAncestor(this),true,0,true,indice);
            janela.setLocationRelativeTo(null);
            janela.setVisible(rootPaneCheckingEnabled);
        }
        
        

    }//GEN-LAST:event_btn_add_vendaActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JDialogListaDeClientes dialog = new JDialogListaDeClientes(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_add_cliente;
    private javax.swing.JButton btn_add_venda;
    private javax.swing.JButton btn_buscar;
    private javax.swing.JButton btn_editar;
    private javax.swing.JButton btn_editar1;
    private javax.swing.JButton btn_limpar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbclientes;
    private javax.swing.JTextField txtbusca;
    // End of variables declaration//GEN-END:variables
}