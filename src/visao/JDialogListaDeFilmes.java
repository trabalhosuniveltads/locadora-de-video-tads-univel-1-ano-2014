package visao;

import DAO.Banco;
import DAO.FilmeDAO;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import objeto.Cliente;
import objeto.Filme;
import objeto.LocacaoItem;
import static visao.Locadora.b;
import static visao.Locadora.tableModelFilmes;

public class JDialogListaDeFilmes extends javax.swing.JDialog {
    
    boolean add_film;

    public JDialogListaDeFilmes(java.awt.Frame parent, boolean modal, boolean add_film) {
        super(parent, modal);
        initComponents();
        
        this.add_film = add_film;
        
        btn_add_filme_locacao.setVisible(add_film);
        
        tbfilmes.setModel(tableModelFilmes);
        
        tableModelFilmes.fireTableDataChanged(); //faz um refresh no Modelo_Filmes
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbfilmes = new javax.swing.JTable();
        btn_add_cliente = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtbusca = new javax.swing.JTextField();
        btn_buscar = new javax.swing.JButton();
        btn_limpar = new javax.swing.JButton();
        btn_editar = new javax.swing.JButton();
        btn_add_filme_locacao = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);

        jLabel2.setFont(new java.awt.Font("Tekton Pro Cond", 1, 48)); // NOI18N
        jLabel2.setText("Filmes");

        tbfilmes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Ativo", "Nome", "CPF"
            }
        ));
        tbfilmes.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tbfilmes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbfilmesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbfilmes);

        btn_add_cliente.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_add_cliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icon-adicionar.png"))); // NOI18N
        btn_add_cliente.setText("Novo Filme");
        btn_add_cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_add_clienteActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel5.setText("Buscar");

        txtbusca.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        btn_buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_find.png"))); // NOI18N
        btn_buscar.setText("Buscar");
        btn_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarActionPerformed(evt);
            }
        });

        btn_limpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_refresh.png"))); // NOI18N
        btn_limpar.setText("Limpar");
        btn_limpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_limparActionPerformed(evt);
            }
        });

        btn_editar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_matricula.png"))); // NOI18N
        btn_editar.setText("Editar");
        btn_editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editarActionPerformed(evt);
            }
        });

        btn_add_filme_locacao.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_add_filme_locacao.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icon-adicionar.png"))); // NOI18N
        btn_add_filme_locacao.setText("Adicionar Filme na Locação!");
        btn_add_filme_locacao.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_add_filme_locacaoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(btn_add_filme_locacao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(18, 18, 18)
                                .addComponent(txtbusca, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btn_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_limpar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_editar))
                            .addComponent(btn_add_cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 281, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(jLabel5))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(btn_add_cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btn_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btn_limpar, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn_editar, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtbusca, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btn_add_filme_locacao, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_add_clienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_add_clienteActionPerformed
        // abrir para adicionar cliente
        JDialogFilme janela = new JDialogFilme((Frame) SwingUtilities.getWindowAncestor(this),true,0,true);
        janela.setLocationRelativeTo(null);
        janela.setVisible(rootPaneCheckingEnabled);
    }//GEN-LAST:event_btn_add_clienteActionPerformed

    
    public void  localizar(){
    String busca = txtbusca.getText().toLowerCase().trim(); 
        List<Integer> ids = new ArrayList<>();
        
        if (busca.equals("")){
            tableModelFilmes.setPesquisa(ids,false);
            //se a busca estiver em branco
            tbfilmes.setModel(tableModelFilmes);
            tableModelFilmes.fireTableDataChanged();
        }else{
            tableModelFilmes.setPesquisa(ids,true);
            HashMap<Integer, Filme> filmes = b.getFilmes();
            
            for (Integer key : filmes.keySet()) {
                Filme filme = filmes.get(key);
                
                String pesquisa = filme.getTitulo() + " " + filme.getAnoLancamento() + " " + filme.getCensura()+ " " + filme.getGenero();
                
                if( pesquisa.toLowerCase().matches("(.*)" + busca + "(.*)") ){
                    ids.add(key);              
                }
            }
        }
        tbfilmes.setModel(tableModelFilmes);
        tableModelFilmes.fireTableDataChanged();
    }
    
    
    private void btn_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarActionPerformed
        // TODO add your handling code here:
       localizar();
        
    }//GEN-LAST:event_btn_buscarActionPerformed

    private void btn_limparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_limparActionPerformed
       // TODO add your handling code here:
       txtbusca.setText("");
       localizar();
       
    }//GEN-LAST:event_btn_limparActionPerformed

    private void tbfilmesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbfilmesMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbfilmesMouseClicked

    private void btn_editarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editarActionPerformed
        if(tbfilmes.getSelectedRowCount() == 0){
            //mensagem de erro
            JOptionPane.showMessageDialog(rootPane, "Selecione um filme para editar"); //mostra uma mensagem de erro
        } else {
            int indice = Integer.parseInt(tbfilmes.getValueAt(tbfilmes.getSelectedRow(), 0).toString());
            JDialogFilme janela = new JDialogFilme((Frame) SwingUtilities.getWindowAncestor(this),true,indice,false);
            janela.setLocationRelativeTo(null);
            janela.setVisible(rootPaneCheckingEnabled);
        }
    }//GEN-LAST:event_btn_editarActionPerformed

    private void btn_add_filme_locacaoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_add_filme_locacaoActionPerformed
        // TODO add your handling code here:
        if(tbfilmes.getSelectedRowCount() == 0){
            //mensagem de erro
            JOptionPane.showMessageDialog(rootPane, "Selecione um filme para adicionar na Locação"); //mostra uma mensagem de erro
        } else {
            //localiza o filme selecionado
            int indice = Integer.parseInt(tbfilmes.getValueAt(tbfilmes.getSelectedRow(), 0).toString());
            
            JDialogLocacao.add_film(indice);
            
        }
    }//GEN-LAST:event_btn_add_filme_locacaoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JDialogListaDeFilmes dialog = new JDialogListaDeFilmes(new javax.swing.JFrame(), true,false);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_add_cliente;
    private javax.swing.JButton btn_add_filme_locacao;
    private javax.swing.JButton btn_buscar;
    private javax.swing.JButton btn_editar;
    private javax.swing.JButton btn_limpar;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbfilmes;
    private javax.swing.JTextField txtbusca;
    // End of variables declaration//GEN-END:variables
}