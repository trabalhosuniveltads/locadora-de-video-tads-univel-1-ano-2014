package visao;

import DAO.Banco;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import objeto.Cliente;
import objeto.Locacao;
import static visao.Locadora.b;
import static visao.Locadora.tableModelClientes;
import static visao.Locadora.tableModelLocacao;

public class JDialogListaDeLocacoes extends javax.swing.JDialog {
    
    public JDialogListaDeLocacoes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
                
        tbLocacoes.setModel(tableModelLocacao);
        
        tableModelLocacao.fireTableDataChanged();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbLocacoes = new javax.swing.JTable();
        btn_add_venda = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtbusca = new javax.swing.JTextField();
        btn_buscar = new javax.swing.JButton();
        btn_limpar = new javax.swing.JButton();
        btn_visualizar = new javax.swing.JButton();
        btn_editar1 = new javax.swing.JButton();
        btn_editar2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);

        jLabel2.setFont(new java.awt.Font("Tekton Pro Cond", 1, 48)); // NOI18N
        jLabel2.setText("Locações");

        tbLocacoes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Ativo", "Nome", "CPF"
            }
        ));
        tbLocacoes.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tbLocacoes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbLocacoesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tbLocacoes);

        btn_add_venda.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_add_venda.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icon-adicionar.png"))); // NOI18N
        btn_add_venda.setText("Nova Locação");
        btn_add_venda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_add_vendaActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel5.setText("Buscar");

        txtbusca.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        btn_buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_find.png"))); // NOI18N
        btn_buscar.setText("Buscar");
        btn_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarActionPerformed(evt);
            }
        });

        btn_limpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_refresh.png"))); // NOI18N
        btn_limpar.setText("Limpar");
        btn_limpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_limparActionPerformed(evt);
            }
        });

        btn_visualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_matricula.png"))); // NOI18N
        btn_visualizar.setText("EDITAR");
        btn_visualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_visualizarActionPerformed(evt);
            }
        });

        btn_editar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_matricula.png"))); // NOI18N
        btn_editar1.setText("Relatório de Locações da última semana");
        btn_editar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editar1ActionPerformed(evt);
            }
        });

        btn_editar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_matricula.png"))); // NOI18N
        btn_editar2.setText("Relatório de Locações com filmes perdidos");
        btn_editar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editar2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addGap(18, 18, 18)
                        .addComponent(txtbusca)
                        .addGap(18, 18, 18)
                        .addComponent(btn_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_limpar))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 227, Short.MAX_VALUE)
                        .addComponent(btn_add_venda, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_visualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btn_editar2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_editar1))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 730, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(84, 84, 84)
                        .addComponent(jLabel5))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btn_visualizar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn_add_venda, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(13, 13, 13)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(btn_limpar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn_buscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtbusca))
                        .addGap(18, 18, 18)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 292, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_editar1, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_editar2, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(17, 17, 17))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_add_vendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_add_vendaActionPerformed
        // TODO add your handling code here:
        JDialogListaDeClientes janela = new JDialogListaDeClientes((Frame) SwingUtilities.getWindowAncestor(this),true);
        janela.setLocationRelativeTo(null);
        janela.setVisible(rootPaneCheckingEnabled);
        
    }//GEN-LAST:event_btn_add_vendaActionPerformed

    public void  localizar(){
        String busca = txtbusca.getText().toLowerCase().trim(); 
        List<Integer> ids = new ArrayList<>();
        
        if (busca.equals("")){
            tableModelLocacao.setPesquisa(ids,false);
            //se a busca estiver em branco
            tbLocacoes.setModel(tableModelLocacao);
            tableModelLocacao.fireTableDataChanged();
        }else{
            tableModelLocacao.setPesquisa(ids,true);
            HashMap<Integer, Locacao> locacoes = b.getLocacoes();
            
            for (Integer key : locacoes.keySet()) {
                Locacao locacao = locacoes.get(key);
                
                String dados_pesquisa = locacao.getCliente().getNome() + " " + locacao.getCliente().getEmail() + " " + locacao.getCliente().getTelefone() + " " +
                       locacao.getCliente().getCPF() + " " + locacao.getCliente().getRG() + " " + locacao.getSituacao() + locacao.getDataLocacao();
                //+ " " + locacao.getItens().getFilme().getTitulo();
                
                if((dados_pesquisa.toLowerCase().matches("(.*)" + busca + "(.*)"))){
                    ids.add(key);              
                }
                
            }
        }
        
        tbLocacoes.setModel(tableModelLocacao);
        tableModelLocacao.fireTableDataChanged();
    }
    
    private void btn_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarActionPerformed
        // TODO add your handling code here:
        localizar();
        
    }//GEN-LAST:event_btn_buscarActionPerformed

    private void btn_limparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_limparActionPerformed
       // TODO add your handling code here:
       txtbusca.setText("");
       localizar();
       
    }//GEN-LAST:event_btn_limparActionPerformed

    private void tbLocacoesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbLocacoesMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tbLocacoesMouseClicked

    private void btn_visualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_visualizarActionPerformed
        // TODO add your handling code here:
        if(tbLocacoes.getSelectedRowCount() == 0){
            //mensagem de erro
            JOptionPane.showMessageDialog(rootPane, "Selecione uma locacao para editar");
        } else {
            int indice = Integer.parseInt(tbLocacoes.getValueAt(tbLocacoes.getSelectedRow(), 0).toString());
            JDialogLocacao janela = new JDialogLocacao((Frame) SwingUtilities.getWindowAncestor(this),true,indice,false, 0);
            janela.setLocationRelativeTo(null);
            janela.setVisible(rootPaneCheckingEnabled);
        }
        
    }//GEN-LAST:event_btn_visualizarActionPerformed

    private void btn_editar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editar1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_editar1ActionPerformed

    private void btn_editar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editar2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_editar2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JDialogListaDeLocacoes dialog = new JDialogListaDeLocacoes(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_add_venda;
    private javax.swing.JButton btn_buscar;
    private javax.swing.JButton btn_editar1;
    private javax.swing.JButton btn_editar2;
    private javax.swing.JButton btn_limpar;
    private javax.swing.JButton btn_visualizar;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tbLocacoes;
    private javax.swing.JTextField txtbusca;
    // End of variables declaration//GEN-END:variables
}