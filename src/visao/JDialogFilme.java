package visao;

import DAO.Banco;
import DAO.ClienteDAO;
import DAO.FilmeDAO;
import DAO.GeneroDAO;
import static com.sun.org.apache.xalan.internal.lib.ExsltDynamic.map;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.swing.JOptionPane;
import modelo.ComboBoxModelFilmesGenero;
import modelo.Funcoes;
import objeto.Cliente;
import objeto.Filme;
import static visao.Locadora.b;
import static visao.Locadora.comboBoxModelFilmesGenero;
import static visao.Locadora.tableModelFilmes;

public class JDialogFilme extends javax.swing.JDialog {
    
    int indice;
    boolean novo = true;
    
    public JDialogFilme(java.awt.Frame parent, boolean modal, int indice, boolean novo) {
        super(parent, modal);
        initComponents();

        this.indice = indice;
        this.novo = novo;
        
        comboBoxModelFilmesGenero = new ComboBoxModelFilmesGenero();
        cb_genero.setModel(comboBoxModelFilmesGenero);
        
        if (!novo){
            //editar
            FilmeDAO filmeDAO = new FilmeDAO();
            Filme filme = filmeDAO.buscar(indice);

            txt_titulo.setText(filme.getTitulo());
            txt_ano_lancamento.setValue(filme.getAnoLancamento());
            txt_qtde.setValue(filme.getQuantidade());
            
            HashMap<Integer, String> generos = b.getGeneros();
            
            
            for (Integer key : generos.keySet()) {
                
                if (filme.getGenero().toString().equals(generos.get(key).toString())){
                    
                    cb_genero.setSelectedItem(generos.get(key));
                }
            }
   
            
            
            //solução mais rápida, am a melhor seria trabalhar com ids
            String ce[] = {"Livre","12 Anos","18 Anos"};
            for ( int count = 0; count < ce.length; count++ ) {
                //Funcoes.mensagem(rootPane, "", filme.getCensura().toString() + " " + count + " " + ce[count]);
                if (filme.getCensura().toString().equals(ce[count].toString())){
                    cb_censura.setSelectedIndex(count);
                }
            }

           


            

        }
        
        
        
    }   
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        txt_titulo = new javax.swing.JTextField();
        btn_salvar = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        cb_censura = new javax.swing.JComboBox();
        cb_genero = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        cs_lancamento = new javax.swing.JCheckBox();
        txt_qtde = new javax.swing.JSpinner();
        txt_ano_lancamento = new javax.swing.JSpinner();

        jButton1.setText("jButton1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Cadastro de Cliente");
        setAlwaysOnTop(true);
        setModal(true);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);

        jLabel2.setFont(new java.awt.Font("Tekton Pro Cond", 1, 48)); // NOI18N
        jLabel2.setText("Cadastro de Filme");

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Título:");

        txt_titulo.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        btn_salvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_salvar.png"))); // NOI18N
        btn_salvar.setText("SALVAR");
        btn_salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salvarActionPerformed(evt);
            }
        });

        btn_cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_cancelar.png"))); // NOI18N
        btn_cancelar.setText("CANCELAR");
        btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel5.setText("Censura: ");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel6.setText("Gênero:");

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel14.setText("Ano de Lançamento: ");

        cb_censura.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        cb_censura.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Livre", "12 Anos", "18 Anos" }));
        cb_censura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_censuraActionPerformed(evt);
            }
        });

        cb_genero.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        cb_genero.setToolTipText("");
        cb_genero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_generoActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Quantidade:");

        cs_lancamento.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        cs_lancamento.setText("Lançamento");
        cs_lancamento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cs_lancamentoActionPerformed(evt);
            }
        });

        txt_qtde.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txt_qtde.setModel(new javax.swing.SpinnerNumberModel(0, 0, 9999, 1));

        txt_ano_lancamento.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txt_ano_lancamento.setModel(new javax.swing.SpinnerNumberModel(2014, 1900, 2015, 1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel1)
                                .addComponent(txt_titulo, javax.swing.GroupLayout.PREFERRED_SIZE, 422, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel14)
                                .addComponent(cs_lancamento)
                                .addComponent(txt_ano_lancamento, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addComponent(jLabel2)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel6)
                                .addComponent(cb_genero, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(27, 27, 27)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel5)
                                .addComponent(cb_censura, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGap(18, 18, 18)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel3)
                                .addComponent(txt_qtde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btn_cancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_salvar, javax.swing.GroupLayout.PREFERRED_SIZE, 201, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(13, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txt_titulo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_ano_lancamento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel14)
                                .addGap(34, 34, 34)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel6)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cb_genero))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(cb_censura, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txt_qtde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addComponent(cs_lancamento, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(34, 34, 34)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_salvar, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_salvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salvarActionPerformed
        if (txt_titulo.getText().isEmpty()) {
            Funcoes.mensagem_foco(rootPane, "Alerta!", "O título é obrigatório", txt_titulo);
        } else if (txt_ano_lancamento.getValue().toString().isEmpty()) {
            Funcoes.mensagem_foco(rootPane, "Alerta!", "O ano de lançamento é obrigatório", txt_ano_lancamento);
        } else {
            try {
                String titulo = txt_titulo.getText();
                int anoLancamento = Integer.parseInt(txt_ano_lancamento.getValue().toString());
                int qtde = Integer.parseInt(txt_qtde.getValue().toString());
                String censura = cb_censura.getSelectedItem().toString();
                String genero = cb_genero.getSelectedItem().toString();
                boolean lancamento = cs_lancamento.isSelected();

                FilmeDAO filmeDAO = new FilmeDAO();
                if (novo){
                    //salvar
                    filmeDAO.gravar(titulo, anoLancamento, qtde, censura, genero, lancamento);

                }else{
                    //editar
                    filmeDAO.alterar_cadastro(indice, titulo, anoLancamento, qtde, censura, genero, lancamento);
                }
                dispose();
            } catch (Exception e) {
                 JOptionPane.showMessageDialog(rootPane,e.getCause().toString());
            }
        
        }
        tableModelFilmes.fireTableDataChanged();       
    }//GEN-LAST:event_btn_salvarActionPerformed

    private void btn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarActionPerformed
        dispose(); //fecha o formulario sem fazer nenhuma alteração
    }//GEN-LAST:event_btn_cancelarActionPerformed

    private void cb_censuraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_censuraActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cb_censuraActionPerformed

    private void cs_lancamentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cs_lancamentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cs_lancamentoActionPerformed

    private void cb_generoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_generoActionPerformed
        
    }//GEN-LAST:event_cb_generoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JDialogFilme.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JDialogFilme.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JDialogFilme.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JDialogFilme.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                JDialogFilme dialog = new JDialogFilme(new javax.swing.JFrame(), true, 0, true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_salvar;
    private javax.swing.JComboBox cb_censura;
    private javax.swing.JComboBox cb_genero;
    private javax.swing.JCheckBox cs_lancamento;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JSpinner txt_ano_lancamento;
    private javax.swing.JSpinner txt_qtde;
    private javax.swing.JTextField txt_titulo;
    // End of variables declaration//GEN-END:variables
}