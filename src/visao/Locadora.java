package visao;

import DAO.Banco;
import DAO.ClienteDAO;
import DAO.FilmeDAO;
import DAO.GeneroDAO;
import DAO.LocacaoDAO;
import DAO.LocadoraDAO;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import modelo.ComboBoxModelClientesUF;
import modelo.ComboBoxModelFilmesCensura;
import modelo.ComboBoxModelFilmesGenero;
import modelo.ComboBoxModelLocacaoCliente;
import modelo.ComboBoxModelLocacaoFilmes;
import modelo.ComboBoxModelLocacaoSituacao;
import modelo.Funcoes;
import modelo.ModeloLocacaoItem;
import modelo.TableModelClientes;
import modelo.TableModelFilmes;
import modelo.TableModelGenero;
import modelo.TableModelLocacao;
import objeto.Cliente;
import objeto.Filme;
import objeto.Locacao;
import org.joda.time.DateMidnight;
import org.joda.time.Interval;
import org.joda.time.Period;

public final class Locadora extends javax.swing.JFrame {
   
    //public static HashMap<Integer, Locacao> locacaoAlterar = new HashMap<>();
    
    static TableModelClientes tableModelClientes = new TableModelClientes();
    static ComboBoxModelClientesUF comboBoxModelClientesUF = new ComboBoxModelClientesUF();
    static TableModelGenero tableModelGenero = new TableModelGenero();
    static ComboBoxModelFilmesCensura comboBoxModelFilmesCensura = new ComboBoxModelFilmesCensura();
    static ComboBoxModelFilmesGenero comboBoxModelFilmesGenero = new ComboBoxModelFilmesGenero();
    static TableModelFilmes tableModelFilmes = new TableModelFilmes();
    static ComboBoxModelLocacaoSituacao comboBoxModelLocacaoSituacao = new ComboBoxModelLocacaoSituacao();
    static TableModelLocacao tableModelLocacao = new TableModelLocacao();
    static ComboBoxModelLocacaoFilmes comboBoxModelLocacaoFilmes = new ComboBoxModelLocacaoFilmes();
    static Banco b;
    
    static ModeloLocacaoItem tableModelLocacaoItem = new ModeloLocacaoItem();
    
    
    public Locadora() {
        initComponents();
        
        Banco.lerArquivo();
        b  = Banco.getInstance();
        
        AtualizarTextos();
        
    }
    
    public void AtualizarTextos(){
        labelTituloLocadora.setText(b.getLocadora().getNome());
        labelTelefoneLocadora.setText(b.getLocadora().getTelefone());
    }
    
    
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButtonSair = new javax.swing.JButton();
        btn_filmes = new javax.swing.JButton();
        btn_clientes = new javax.swing.JButton();
        btn_configuracoes = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        labelTelefoneLocadora = new javax.swing.JLabel();
        jButton5 = new javax.swing.JButton();
        labelTituloLocadora = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);

        jButtonSair.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_sair.png"))); // NOI18N
        jButtonSair.setText("SAIR");
        jButtonSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSairActionPerformed(evt);
            }
        });

        btn_filmes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/ico_filmes.png"))); // NOI18N
        btn_filmes.setText("Cadastro de Filmes");
        btn_filmes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_filmesActionPerformed(evt);
            }
        });

        btn_clientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_cliente.png"))); // NOI18N
        btn_clientes.setText("Cadastro de Clientes");
        btn_clientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_clientesActionPerformed(evt);
            }
        });

        btn_configuracoes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/ico_config.png"))); // NOI18N
        btn_configuracoes.setText("Configurações do Sistema");
        btn_configuracoes.setToolTipText("");
        btn_configuracoes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_configuracoesActionPerformed(evt);
            }
        });
        btn_configuracoes.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                atualizar_tela(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/cart.png"))); // NOI18N
        jButton4.setText("Locações");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        labelTelefoneLocadora.setFont(new java.awt.Font("Tekton Pro Cond", 1, 36)); // NOI18N
        labelTelefoneLocadora.setText("(45)1234-1234");

        jButton5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/ico_folder.png"))); // NOI18N
        jButton5.setText("Generos");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        labelTituloLocadora.setFont(new java.awt.Font("Tekton Pro Cond", 1, 48)); // NOI18N
        labelTituloLocadora.setText("Sisteminha de Vendas");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelTituloLocadora)
                    .addComponent(labelTelefoneLocadora)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btn_clientes, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_filmes, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btn_configuracoes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jButtonSair, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(20, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(15, Short.MAX_VALUE)
                .addComponent(labelTituloLocadora)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(labelTelefoneLocadora)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_clientes, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton5, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jButtonSair, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_configuracoes, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_filmes, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(29, 29, 29))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSairActionPerformed
        System.exit(0); //fechar a aplicação
    }//GEN-LAST:event_jButtonSairActionPerformed

    private void btn_filmesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_filmesActionPerformed
        // abrir a janela de Lista de Filmes
        JDialogListaDeFilmes janela = new JDialogListaDeFilmes(this,true,false);
        janela.setLocationRelativeTo(null);
        janela.setVisible(rootPaneCheckingEnabled);
    }//GEN-LAST:event_btn_filmesActionPerformed

    private void btn_clientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_clientesActionPerformed
        // abrir a janela de Lista de Clientes
        JDialogListaDeClientes janela = new JDialogListaDeClientes(this,true);
        janela.setLocationRelativeTo(null);
        janela.setVisible(rootPaneCheckingEnabled);
    }//GEN-LAST:event_btn_clientesActionPerformed

    private void btn_configuracoesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_configuracoesActionPerformed
        // abrir a janela de Configurações
        JDialogConfiguracao janela = new JDialogConfiguracao(this,true);
        janela.setLocationRelativeTo(null);
        janela.setVisible(rootPaneCheckingEnabled);       
    }//GEN-LAST:event_btn_configuracoesActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // abrir a janela de Lista de Locações
        JDialogListaDeLocacoes janela = new JDialogListaDeLocacoes(this,true);
        janela.setLocationRelativeTo(null);
        janela.setVisible(rootPaneCheckingEnabled);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed

        // abrir a janela de Configurações
        JDialogListaDeGeneros janela = new JDialogListaDeGeneros(this,true);
        janela.setLocationRelativeTo(null);
        janela.setVisible(rootPaneCheckingEnabled); 
    }//GEN-LAST:event_jButton5ActionPerformed

    private void atualizar_tela(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_atualizar_tela
        // TODO add your handling code here:
        AtualizarTextos();
    }//GEN-LAST:event_atualizar_tela

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Locadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Locadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Locadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Locadora.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new Locadora().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_clientes;
    private javax.swing.JButton btn_configuracoes;
    private javax.swing.JButton btn_filmes;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButtonSair;
    private javax.swing.JLabel labelTelefoneLocadora;
    private javax.swing.JLabel labelTituloLocadora;
    // End of variables declaration//GEN-END:variables
    
    
}