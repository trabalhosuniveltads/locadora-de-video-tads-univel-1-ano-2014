package visao;

import DAO.ClienteDAO;
import DAO.FilmeDAO;
import DAO.LocacaoDAO;
import java.awt.Frame;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import modelo.Funcoes;
import modelo.ModeloLocacaoItem;
import objeto.Cliente;
import objeto.Filme;
import objeto.Locacao;
import objeto.LocacaoItem;
import static visao.Locadora.b;
import static visao.Locadora.tableModelLocacao;
import static visao.Locadora.tableModelLocacaoItem;

public class JDialogLocacao extends javax.swing.JDialog {
    
    int indice;
    boolean novo = true;
    Cliente cliente;
    Date dataLocacao;
    String situacao = "Pendente";
    float valorTotal = 0;
    float valorTotalPago = 0;
    float valorMulta = 0;
    public static HashMap<Integer, LocacaoItem> itens = new HashMap<>();
    int qtdItens = 0;
    
    JDialogLocacao(java.awt.Frame parent, boolean modal, int indice, boolean novo, int cliente_indice) {
        super(parent, modal);
        initComponents();
        itens.clear();
        this.indice = indice;
        this.novo = novo;
        
        if (novo){
            // novo
            ClienteDAO clienteDAO = new ClienteDAO();
            cliente = (Cliente) clienteDAO.buscar(cliente_indice);
            txt_dataLocacao.setText( "Data da Locação: " + dataAtual().toString());
        }else{
            //editar
            LocacaoDAO locacaoDAO = new LocacaoDAO();
            Locacao locacao = locacaoDAO.buscar(indice);
            //seta o cliente
            cliente = (Cliente) locacao.getCliente();
            itens.putAll(locacao.getItens());
            
            dataLocacao = locacao.getDataLocacao();
            txt_dataLocacao.setText(dataLocacao.toString());
            situacao = locacao.getSituacao();
            valorTotal = locacao.getValorTotal();
            valorTotalPago = locacao.getValorTotalPago();
            valorMulta = locacao.getValorMulta();
        }
        
        //arruma a visão
        UpdateVisao();
        
        
        txt_nome_cliente.setText("Cliente: " + cliente.getNome());
    }
    
    private String dataAtual(){
        Date data = new Date(System.currentTimeMillis());    
        SimpleDateFormat formatarDate = new SimpleDateFormat("dd/MM/yyyy");   
        return formatarDate.format(data);
    }
   
    
    private void UpdateVisao(){
        
        // atualiza os valores do pedido
        float total_inicial = 0;
        float total = 0;
        int itens_ok = 0; // para comparar com o total de itens

        for (Integer key : itens.keySet()) {
            LocacaoItem i = itens.get(key);
            String sit = i.getSituacao().toLowerCase();
            float valtot = i.getValorTotal();
            float valtotpgo = i.getValorTotalPago();
            int atrz = i.getAtrazo();
            
            if (!sit.equals("cancelado")){
                //total inicial
                total_inicial = total_inicial + valtot;
                //total pago
                total += (valtotpgo > 0) ? valtotpgo : valtot;
                qtdItens++;
            if(!sit.equals("pendente")){
                itens_ok++;
            }
            }

        }

        valorTotal = total_inicial;
        valorTotalPago = total;
        valorMulta = total - total_inicial;

        //verificar situacao do pedido
        if(itens_ok == qtdItens){
            situacao = "Finalizado";
        }else{
            situacao = "Pendente";
        }

        
        txt_valorLocacao.setText("Valor dos Filmes: R$ " + valorTotal);
        txt_valorMulta.setText("Valor da Multa: R$ " + valorMulta);
        txt_valorTotalPagar.setText("Valor Total: R$ " + (valorTotal + valorMulta));
        
        tableModelLocacaoItem.setLista(itens);
        tbitens.setModel(tableModelLocacaoItem);
        tableModelLocacaoItem.fireTableDataChanged();
        
    }
   
    public static void add_film(int indice){
        
        //bisca os dados do filme (para pegar o valor e verificar se esta disponivel em estoque
        FilmeDAO filmeDAO = new FilmeDAO();
        Filme filme = filmeDAO.buscar(indice);
        
        //coleta informações do sistema
        int acrescenta_dias = filme.isLancamento() ? b.getLocadora().getDiasLocacaoFilmeLancamento() : b.getLocadora().getDiasLocacaoFilme();
        float valor_filme = filme.isLancamento() ? (float) b.getLocadora().getValorDiariaFilmeLancamento() : (float) b.getLocadora().getValorDiariaFilme();
        
        //pega a data de agora e adiciona X dias (dependendo do filme)
        Calendar dataDev = Calendar.getInstance();
        dataDev.setTime(new Date());
        Date DataEmprestimo = (Date) dataDev.getTime();
        dataDev.add(Calendar.DATE, acrescenta_dias);
        Date DataDevolucao = (Date) dataDev.getTime();
        
        ///Filme filme, Date dataEmprestimo, int atrazo, String situacao, float valorTotal, float valorTotalPago
        LocacaoItem item = new LocacaoItem(filme, DataEmprestimo, DataDevolucao,0, "Pendente", (float) valor_filme, 0);
        
        itens.put(indice, item);
        
        //tableModelLocacaoItem.setLista(itens);
        tableModelLocacaoItem.setLista(itens);
        tableModelLocacaoItem.fireTableDataChanged();
        
        //Funcoes.mensagem(null, "", item.getFilme().getTitulo());
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList();
        jScrollPane3 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();
        jLabel2 = new javax.swing.JLabel();
        txt_nome_cliente = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        btn_salvar = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbitens = new javax.swing.JTable();
        btn_remover_item1 = new javax.swing.JButton();
        btn_remover_item2 = new javax.swing.JButton();
        btn_remover_item3 = new javax.swing.JButton();
        txt_dataLocacao = new javax.swing.JLabel();
        txt_valorLocacao = new javax.swing.JLabel();
        btn_add_filme = new javax.swing.JButton();
        btn_remover_item4 = new javax.swing.JButton();
        txt_valorMulta = new javax.swing.JLabel();
        txt_valorTotalPagar = new javax.swing.JLabel();

        jButton1.setText("jButton1");

        jList1.setModel(new javax.swing.AbstractListModel() {
            String[] strings = { "Item 1", "Item 2", "Item 3", "Item 4", "Item 5" };
            public int getSize() { return strings.length; }
            public Object getElementAt(int i) { return strings[i]; }
        });
        jScrollPane2.setViewportView(jList1);

        jTextArea1.setColumns(20);
        jTextArea1.setRows(5);
        jScrollPane3.setViewportView(jTextArea1);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);

        jLabel2.setFont(new java.awt.Font("Tekton Pro Cond", 1, 48)); // NOI18N
        jLabel2.setText("Locação");

        txt_nome_cliente.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txt_nome_cliente.setText("Cliente: João da Silva");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Item(s) da locação");

        btn_salvar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_salvar.png"))); // NOI18N
        btn_salvar.setText("SALVAR");
        btn_salvar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salvarActionPerformed(evt);
            }
        });

        btn_cancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_cancelar.png"))); // NOI18N
        btn_cancelar.setText("CANCELAR EDIÇÃO");
        btn_cancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelarActionPerformed(evt);
            }
        });

        tbitens.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tbitens.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(tbitens);

        btn_remover_item1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_remover_item1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/ico_devolvido.png"))); // NOI18N
        btn_remover_item1.setText("DEVOLVER FILME");
        btn_remover_item1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_remover_item1ActionPerformed(evt);
            }
        });

        btn_remover_item2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_remover_item2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/ico_cancelar2.png"))); // NOI18N
        btn_remover_item2.setText("CANCELAR LOCACAO DO FILME");
        btn_remover_item2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_remover_item2ActionPerformed(evt);
            }
        });

        btn_remover_item3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_remover_item3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_remover.png"))); // NOI18N
        btn_remover_item3.setText("O CLIENTE PERDEU O FILME");
        btn_remover_item3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_remover_item3ActionPerformed(evt);
            }
        });

        txt_dataLocacao.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txt_dataLocacao.setText("data da locação: 01/01/2000");

        txt_valorLocacao.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txt_valorLocacao.setText("valor da Locação: R$ 0,00");

        btn_add_filme.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_add_filme.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icon-adicionar.png"))); // NOI18N
        btn_add_filme.setText("Adicionar Filme");
        btn_add_filme.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_add_filmeActionPerformed(evt);
            }
        });

        btn_remover_item4.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        btn_remover_item4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/ico_cancelar2.png"))); // NOI18N
        btn_remover_item4.setText("COLOCAR COMO PENDENTE");
        btn_remover_item4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_remover_item4ActionPerformed(evt);
            }
        });

        txt_valorMulta.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txt_valorMulta.setText("valor da Multa: R$ 0,00");

        txt_valorTotalPagar.setFont(new java.awt.Font("Tahoma", 0, 48)); // NOI18N
        txt_valorTotalPagar.setText("valor da Locação: R$ 0,00");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txt_valorLocacao)
                                .addGap(18, 18, 18)
                                .addComponent(txt_valorMulta)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_add_filme, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_nome_cliente)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(95, 95, 95)
                                .addComponent(txt_valorTotalPagar))
                            .addComponent(txt_dataLocacao)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(btn_remover_item4, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(269, 269, 269))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(btn_remover_item2, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(btn_remover_item1, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(btn_remover_item3, javax.swing.GroupLayout.PREFERRED_SIZE, 307, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addGap(18, 18, 18)
                                        .addComponent(btn_cancelar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                                .addComponent(btn_salvar, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 9, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(txt_valorTotalPagar))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_nome_cliente)
                .addGap(7, 7, 7)
                .addComponent(txt_dataLocacao)
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_valorLocacao)
                            .addComponent(txt_valorMulta))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel3))
                    .addComponent(btn_add_filme, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btn_remover_item1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_remover_item4, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_remover_item3)
                        .addGap(6, 6, 6)
                        .addComponent(btn_remover_item2))
                    .addComponent(btn_salvar, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cancelar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_salvarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salvarActionPerformed
      try {
            
            LocacaoDAO locacaoDAO = new LocacaoDAO();
            
            if (novo){
                //salvar
                locacaoDAO.gravar(cliente, new HashMap<Integer, LocacaoItem>(itens), new Date(System.currentTimeMillis()), situacao, valorTotal, valorTotalPago, itens.size());
            }else{
                //editar
                locacaoDAO.alterar_cadastro(indice, cliente, new HashMap<Integer, LocacaoItem>(itens), dataLocacao, situacao, valorTotal, valorTotalPago, itens.size());
            }

            tableModelLocacao.fireTableDataChanged();
            dispose();
       
        } catch (Exception e) {
             JOptionPane.showMessageDialog(rootPane,e.getCause().toString());
    }
        
    }//GEN-LAST:event_btn_salvarActionPerformed

    private void btn_cancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelarActionPerformed
        // TODO add your handling code here:
        dispose(); //fecha o formulário sem fazer nenhuma alteração
    }//GEN-LAST:event_btn_cancelarActionPerformed

    
    public void atualizaSituacaoItem(String nova_situacao){
        if(tbitens.getSelectedRowCount() == 0){
            //mensagem de erro
            JOptionPane.showMessageDialog(rootPane, "Selecione um item");
        } else {
            int indice = Integer.parseInt(tbitens.getValueAt(tbitens.getSelectedRow(), 0).toString());
            LocacaoItem item_base = itens.get(indice);
            //(Filme filme, Date dataEmprestimo, Date dataDevolucao, int atrazo, String situacao, float valorTotal, float valorTotalPago
            LocacaoItem item = new LocacaoItem (item_base.getFilme(),item_base.getDataEmprestimo(),item_base.getDataDevolucao(),
            item_base.getAtrazo(),item_base.getSituacao(),item_base.getValorTotal(),item_base.getValorTotalPago());
            
            if (item_base.getSituacao() == nova_situacao){
                Funcoes.mensagem(rootPane, "Alerta!", "o filme já está com essa situação.");
            }else{
                // sweta a nova situacao
                item.setSituacao(nova_situacao);

                // caso o filme seja perdido = multa de 50 reais (deposis colocar no sistema
                if (item.getSituacao() == "Perdido"){
                    item.setValorTotalPago((float) 50);
                    item.setAtrazo(0);
                }

                // caso o filme seja cancelado = nao será cobrado
                if (item.getSituacao() == "Pendente"){
                    item.setValorTotalPago((float) 0);
                    item.setAtrazo(0);
                }


                // caso o filme seja cancelado = nao será cobrado
                if (item.getSituacao() == "Cancelado"){
                    item.setValorTotalPago((float) 0);
                    item.setAtrazo(0);
                }


                // caso o filme seja devolvido
                if (item.getSituacao() == "Devolvido"){

                    //diferença entre datas
                    int dias = Funcoes.diferencaEmDias(item.getDataDevolucao(), new Date(System.currentTimeMillis()));

                    //System.out.println(" DIFERENCA de "+dias);

                    if (dias > 0){
                        item.setAtrazo(dias);
                        item.setValorTotalPago( (dias * (float) b.getLocadora().getValorMultaDia()) + item.getValorTotal());
                        //System.out.println( "vai pagar multa de " + dias * (float) b.getLocadora().getValorMultaDia());

                    }else{
                        item.setAtrazo(0);
                        item.setValorTotalPago(item.getValorTotal());
                        //System.out.println( "vai pagar normal " + (float) b.getLocadora().getValorMultaDia());
                    }

                }

                //substitui o item da lista
                itens.put(indice, item);


                //arruma a visão
                UpdateVisao();
            }
        }
    }
    
    
    
    
    
    private void btn_remover_item1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_remover_item1ActionPerformed
        // TODO add your handling code here:
        atualizaSituacaoItem("Devolvido");
    }//GEN-LAST:event_btn_remover_item1ActionPerformed

    private void btn_remover_item2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_remover_item2ActionPerformed
        // TODO add your handling code here:
        atualizaSituacaoItem("Cancelado");
    }//GEN-LAST:event_btn_remover_item2ActionPerformed

    private void btn_remover_item3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_remover_item3ActionPerformed
        // TODO add your handling code here:
        atualizaSituacaoItem("Perdido");
    }//GEN-LAST:event_btn_remover_item3ActionPerformed

    private void btn_add_filmeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_add_filmeActionPerformed
        // TODO add your handling code here:
        JDialogListaDeFilmes janela = new JDialogListaDeFilmes((Frame) SwingUtilities.getWindowAncestor(this),true,true);
        janela.setLocationRelativeTo(null);
        janela.setVisible(rootPaneCheckingEnabled);
    }//GEN-LAST:event_btn_add_filmeActionPerformed

    private void btn_remover_item4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_remover_item4ActionPerformed
        // TODO add your handling code here:
        atualizaSituacaoItem("Pendente");
    }//GEN-LAST:event_btn_remover_item4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JDialogLocacao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JDialogLocacao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JDialogLocacao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JDialogLocacao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        
        
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_add_filme;
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_remover_item1;
    private javax.swing.JButton btn_remover_item2;
    private javax.swing.JButton btn_remover_item3;
    private javax.swing.JButton btn_remover_item4;
    private javax.swing.JButton btn_salvar;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JList jList1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTextArea jTextArea1;
    private javax.swing.JTable tbitens;
    private javax.swing.JLabel txt_dataLocacao;
    private javax.swing.JLabel txt_nome_cliente;
    private javax.swing.JLabel txt_valorLocacao;
    private javax.swing.JLabel txt_valorMulta;
    private javax.swing.JLabel txt_valorTotalPagar;
    // End of variables declaration//GEN-END:variables
}