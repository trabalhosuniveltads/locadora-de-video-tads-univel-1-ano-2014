package visao;

import DAO.Banco;
import DAO.GeneroDAO;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.RowFilter.Entry;
import javax.swing.SwingUtilities;
import modelo.Funcoes;
import modelo.TableModelGenero;
import objeto.Cliente;
import static visao.Locadora.b;
import static visao.Locadora.tableModelGenero;

public class JDialogListaDeGeneros extends javax.swing.JDialog {
    

    public JDialogListaDeGeneros(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tabelaGeneros.setModel(tableModelGenero);
        
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tabelaGeneros = new javax.swing.JTable();
        btn_novo = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        txtbusca = new javax.swing.JTextField();
        btn_buscar = new javax.swing.JButton();
        btn_limpar = new javax.swing.JButton();
        btn_editar = new javax.swing.JButton();
        btn_apagar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        setType(java.awt.Window.Type.UTILITY);

        jLabel2.setFont(new java.awt.Font("Tekton Pro Cond", 1, 48)); // NOI18N
        jLabel2.setText("Generos");

        tabelaGeneros.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Ativo", "Nome", "CPF"
            }
        ));
        tabelaGeneros.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tabelaGeneros.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tabelaGenerosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tabelaGeneros);

        btn_novo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_novo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icon-adicionar.png"))); // NOI18N
        btn_novo.setText("Novo Genero");
        btn_novo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_novoActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel5.setText("Buscar");

        txtbusca.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        btn_buscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_find.png"))); // NOI18N
        btn_buscar.setText("Buscar");
        btn_buscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscarActionPerformed(evt);
            }
        });

        btn_limpar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_refresh.png"))); // NOI18N
        btn_limpar.setText("Limpar");
        btn_limpar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_limparActionPerformed(evt);
            }
        });

        btn_editar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_matricula.png"))); // NOI18N
        btn_editar.setText("Editar");
        btn_editar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_editarActionPerformed(evt);
            }
        });

        btn_apagar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagens/icone_remover.png"))); // NOI18N
        btn_apagar.setText("Apagar");
        btn_apagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_apagarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(18, 18, 18)
                                .addComponent(txtbusca, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btn_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_limpar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_editar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btn_apagar))
                            .addComponent(btn_novo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(84, 84, 84)
                .addComponent(jLabel5)
                .addGap(445, 445, 445))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(btn_novo, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(14, 14, 14)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(btn_limpar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_buscar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtbusca)
                    .addComponent(btn_apagar, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_editar, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_novoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_novoActionPerformed
        // abrir para adicionar cliente
        JDialogGenero form = new JDialogGenero((Frame) SwingUtilities.getWindowAncestor(this),true,0,true);
        form.setLocationRelativeTo(null);
        form.setVisible(rootPaneCheckingEnabled);
    }//GEN-LAST:event_btn_novoActionPerformed

    private void btn_buscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscarActionPerformed
        localizar();
    }//GEN-LAST:event_btn_buscarActionPerformed
    
    public void  localizar(){
    String busca = txtbusca.getText().toLowerCase().trim(); 
        List<Integer> ids = new ArrayList<>();
        
        if (busca.equals("")){
            tableModelGenero.setPesquisa(ids,false);
            //se a busca estiver em branco
            tabelaGeneros.setModel(tableModelGenero);
            tableModelGenero.fireTableDataChanged();
        }else{
            HashMap<Integer, String> genero = b.getGeneros();
            
            for (Integer key : genero.keySet()) {
                String nome = genero.get(key).toString();
                if((nome.toLowerCase().matches("(.*)" + busca + "(.*)"))){
                    ids.add(key);              
                }
            }
            tableModelGenero.setPesquisa(ids,true);
        }
        tabelaGeneros.setModel(tableModelGenero);
        tableModelGenero.fireTableDataChanged();
    }
    
    
    private void btn_limparActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_limparActionPerformed
       // TODO add your handling code here:
       txtbusca.setText("");
       localizar();

    }//GEN-LAST:event_btn_limparActionPerformed

    private void tabelaGenerosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tabelaGenerosMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_tabelaGenerosMouseClicked

    private void btn_editarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_editarActionPerformed
        // TODO add your handling code here:
        if(tabelaGeneros.getSelectedRowCount() == 0){
            //mensagem de erro
            JOptionPane.showMessageDialog(rootPane, "Selecione um Gênero para editá-lo"); //mostra uma mensagem de erro
        } else {
            int indice = Integer.parseInt(tabelaGeneros.getValueAt(tabelaGeneros.getSelectedRow(), 0).toString());
            JDialogGenero janela = new JDialogGenero((Frame) SwingUtilities.getWindowAncestor(this),true,indice, false);
            janela.setLocationRelativeTo(null);
            janela.setVisible(rootPaneCheckingEnabled);
        }
    }//GEN-LAST:event_btn_editarActionPerformed

    private void btn_apagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_apagarActionPerformed
        // TODO add your handling code here:
        if(tabelaGeneros.getSelectedRowCount() == 0){
            //mensagem de erro
            JOptionPane.showMessageDialog(rootPane, "Selecione um Gênero para apagar."); //mostra uma mensagem de erro
        } else {
            int indice = Integer.parseInt(tabelaGeneros.getValueAt(tabelaGeneros.getSelectedRow(), 0).toString());
            int confirmacao = JOptionPane.showConfirmDialog (null, "Quer realmente apagar o registro?","Confirmação", JOptionPane.YES_OPTION);
            if(confirmacao == 0){
                GeneroDAO generoDAO = new GeneroDAO();
                //apagar
                generoDAO.apagar(indice);
                tableModelGenero.fireTableDataChanged();
            }            
        }
        
    }//GEN-LAST:event_btn_apagarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                JDialogListaDeGeneros dialog = new JDialogListaDeGeneros(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_apagar;
    private javax.swing.JButton btn_buscar;
    private javax.swing.JButton btn_editar;
    private javax.swing.JButton btn_limpar;
    private javax.swing.JButton btn_novo;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable tabelaGeneros;
    private javax.swing.JTextField txtbusca;
    // End of variables declaration//GEN-END:variables
}