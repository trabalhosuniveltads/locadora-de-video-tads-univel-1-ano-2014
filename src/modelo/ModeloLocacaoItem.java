package modelo;

import DAO.Banco;
import DAO.LocacaoDAO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import objeto.Locacao;
import objeto.LocacaoItem;

public class ModeloLocacaoItem extends AbstractTableModel {

    private List<Integer> ids = new ArrayList<>();    
    private HashMap<Integer, LocacaoItem> lista = new HashMap();
    
    int indice = 0;
    
    public void setLista(HashMap<Integer, LocacaoItem> lista){
        this.lista.clear();
        this.lista.putAll(lista);
        System.out.println("LISTA DE FILMES DO PEDIDO: " + this.lista); //TODO: remover
    }
    
    @Override
    public int getRowCount() {
     //para comportar os apagados
        List<Integer> ids_active = new ArrayList<>();
        
        for (Integer key : lista.keySet()) {
            LocacaoItem item = lista.get(key);
            if(!item.getSituacao().toLowerCase().equals("")){
                ids_active.add(key);              
            }
        }
        setPesquisa(ids_active);
        return ids.size();
    }
    
    public void setPesquisa(List<Integer> ids) {
        this.ids = ids;
    }
    
    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        
        int row = ids.isEmpty() ? rowIndex : ids.get(rowIndex);
        LocacaoItem item = lista.get(row);
        switch (columnIndex) {
            case 0:
                return row;
            case 1:
                return item.getSituacao();
            case 2:
                return item.getFilme().getTitulo();
            case 3:
                return item.getDataDevolucao();
            case 4:
                return item.getValorTotal();
            case 5:
                return item.getValorTotalPago();
            default:
                return "";
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Item n.";
            case 1:
                return "Situação";
            case 2:
                return "Título";
            case 3:
                return "Data Devolucao";
            case 4:
                return "Total";
            case 5:
                return "Total a Pagar";
            default:
                return "";
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}