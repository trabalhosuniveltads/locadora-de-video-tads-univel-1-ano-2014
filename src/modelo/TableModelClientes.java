package modelo;

import DAO.Banco;
import DAO.ClienteDAO;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import objeto.Cliente;

public class TableModelClientes extends AbstractTableModel{
        
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private List<Integer> ids = new ArrayList<>();    
    private boolean pesquisa;
    
    public void setPesquisa(List<Integer> ids, boolean pesquisa) {
        this.ids = ids;
        this.pesquisa = pesquisa;
    }
    
    @Override
    public int getRowCount() {
        return ids.isEmpty() && !pesquisa ? Banco.getInstance().getClientes().size() : ids.size();
    }

    @Override
    public int getColumnCount() {
        return 14;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Banco b = Banco.getInstance();
        int row = ids.isEmpty() ? rowIndex : ids.get(rowIndex);
        Cliente cliente = b.getClientes().get(Integer.valueOf(row));
        
        switch (columnIndex) {
            case 0:
                return row;
            case 1:
                return cliente.getNome();
            case 2:
                return cliente.getCPF();
            case 3:
                return cliente.getRG();
            case 4:
                return sdf.format(cliente.getDataNascimento());
            case 5:
                return cliente.getLogradouro();
            case 6:
                return cliente.getNumero();
            case 7:
                return cliente.getComplemento();
            case 8:
                return cliente.getCEP();
            case 9:
                return cliente.getBairro();
            case 10:
                return cliente.getCidade();
            case 11:
                return cliente.getEstado();
            case 12:
                return cliente.getTelefone();
            case 13:
                return cliente.getEmail();
            default:
                return "";
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID";
            case 1:
                return "Nome";
            case 2:
                return "CPF";
            case 3:
                return "RG";
            case 4:
                return "Data Nascimento";
            case 5:
                return "Logradouro";
            case 6:
                return "Número";
            case 7:
                return "Complemento";
            case 8:
                return "CEP";
            case 9:
                return "Bairro";
            case 10:
                return "Cidade";
            case 11:
                return "Estado";
            case 12:
                return "Telefone";
            case 13:
                return "E-mail";
            default:
                return "";
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex != 0;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        ClienteDAO clienteDAO = new ClienteDAO();
        
        if (((String) aValue).equals(""))
            JOptionPane.showMessageDialog(null, "Preenchimento obrigatório");
        else {
            switch(columnIndex){
            case 1:
            case 2:
            case 3:
            case 4:
                 try {
                    Date data = sdf.parse((String) aValue);
                    if (sdf.format(data).equals((String) aValue)) {
                        clienteDAO.alterar(columnIndex, rowIndex, data);                   
                    }
                    break;
                } catch (ParseException ex) {}
            case 5:
            case 6:
            case 7:
            case 8:
//                if (String.valueOf(((String)aValue).charAt(5)).contains("-") && ((String)aValue).length() == 9) {
//                    clienteDAO.alterar(columnIndex, rowIndex, aValue);                   
//                }
//                break;
            case 9:
            case 10:
                clienteDAO.alterar(columnIndex, rowIndex, aValue);
                break;
            case 11:
                for (String UF : ComboBoxModelClientesUF.UF) 
                    if (((String) aValue).equals(UF)) {
                        clienteDAO.alterar(columnIndex, rowIndex, aValue);
                        break;
                    }
                break;
            case 12:
            case 13:
            }
        }
        fireTableCellUpdated(rowIndex, columnIndex);
    }
   
}
