package modelo;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class ComboBoxModelClientesUF extends AbstractListModel implements ComboBoxModel{
    
    public static final String[] UF      
    = {"AC","AL","AM","AP","BA","CE","DF","ES","GO","MA","MG","MS","MT","PA","PB","PE","PI","PR","RJ","RN","RO","RR","RS","SC","SE","SP","TO"};
    
    private String estado = "";
    
    // QUAL É A QUANTIDADE DE OPÇÕES NO COMBOBOX 
    @Override
    public int getSize() {
        return UF.length;
    }

    //QUAL O ELEMENTO A SER SELECIONADO
    @Override
    public Object getElementAt(int index) { 
        return UF[index];
    }

    //PEGA O ELEMENTO SELECIONADO E JOGA NA VARIAVEL
    @Override
    public void setSelectedItem(Object anItem) {
        estado = (String) anItem;
    }
    
    @Override
    public Object getSelectedItem() {
        return estado;
    }
}
