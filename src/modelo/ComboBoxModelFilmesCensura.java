package modelo;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class ComboBoxModelFilmesCensura extends AbstractListModel implements ComboBoxModel{
    
    public static final String[]    censura     = {"Livre", "12 Anos", "18 Anos"};
    private String      selecionado = "";
    
    // QUAL É A QUANTIDADE DE OPÇÕES NO COMBOBOX 
    @Override
    public int getSize() {
        return censura.length;
    }

    //QUAL O ELEMENTO A SER SELECIONADO
    @Override
    public Object getElementAt(int index) { 
        return censura[index];
    }

    //PEGA O ELEMENTO SELECIONADO E JOGA NA VARIAVEL
    @Override
    public void setSelectedItem(Object anItem) {
        selecionado = (String) anItem;
    }
    
    @Override
    public Object getSelectedItem() {
        return (String) selecionado;
    }
}
