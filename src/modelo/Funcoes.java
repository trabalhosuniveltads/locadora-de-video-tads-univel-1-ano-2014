package modelo;

import java.awt.Dialog;
import java.awt.Frame;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.xml.crypto.Data;

public class Funcoes {
  
    //formato R$
    public static String formatarValor(double value) {
        Locale brasil = new Locale("pt","BR");  
        DecimalFormatSymbols real = new DecimalFormatSymbols(brasil);  
        DecimalFormat dinheiro = new DecimalFormat("¤ ###,###,##0.00",real);
        return dinheiro.format(value);
    }
    
    
    //mostrar uma mensagem 
    static public void mensagem(javax.swing.JRootPane frame, String titulo, String texto) {
        JOptionPane.showMessageDialog(frame, texto);
    }
    
    //mostrar uma mensagem 
    static public void mensagem_foco(javax.swing.JRootPane frame, String titulo, String texto, JComponent componente) {
        JOptionPane.showMessageDialog(frame, texto);
        componente.requestFocus();
    }
    
    public static boolean isEmailValid(String email) {
        if ((email == null) || (email.trim().length() == 0))
            return false;

        String emailPattern = "\\b(^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@([A-Za-z0-9-])+(\\.[A-Za-z0-9-]+)*((\\.[A-Za-z0-9]{2,})|(\\.[A-Za-z0-9]{2,}\\.[A-Za-z0-9]{2,}))$)\\b";
        Pattern pattern = Pattern.compile(emailPattern, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    
    public static String formatarDataString(Data data){
        SimpleDateFormat formatarDate = new SimpleDateFormat("dd/MM/yyyy");   
        return formatarDate.format(data);
    } 
    
    public static int diferencaEmDias(Date dataInicial, Date dataFinal){  
        double result = 0;  
        long diferenca = dataFinal.getTime() - dataInicial.getTime();  
        double diferencaEmDias = (diferenca /1000) / 60 / 60 /24; 
        return (int) diferencaEmDias;  
    }
    
    public static boolean compararDatas(Date data, Date inicio, Date fim){
        return (data.after(inicio) && data.before(fim));
    }
    
      
}