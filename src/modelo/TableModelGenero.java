package modelo;

import DAO.Banco;
import DAO.GeneroDAO;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

public class TableModelGenero extends AbstractTableModel{
        
    private List<Integer> ids = new ArrayList<>();    
    private boolean pesquisa;
    
    public void setPesquisa(List<Integer> ids, boolean pesquisa) {
        this.ids = ids;
        this.pesquisa = pesquisa;
    }
    
    @Override
    public int getRowCount() {
        //para comportar os apagados
        List<Integer> ids_active = new ArrayList<>();
        Banco b = Banco.getInstance();
            HashMap<Integer, String> genero = b.getGeneros();
            
            for (Integer key : genero.keySet()) {
                String nome = genero.get(key);
                if(!nome.toLowerCase().equals("")){
                    ids_active.add(key);
                }
            }
            setPesquisa(ids_active,true);
        return ids.size();        
        
        //return ids.isEmpty() && !pesquisa ? Banco.getInstance().getGeneros().size() : ids.size();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Banco b = Banco.getInstance();
        
        int row = ids.isEmpty() ? rowIndex : ids.get(rowIndex);
        
        String genero = b.getGeneros().get(Integer.valueOf(row));
        
        switch (columnIndex) {
            case 0:
                return row;
            case 1:
                return genero;
            default:
                return "";
        }
    }
    
    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID";
            case 1:
                return "Gênero";
            default:
                return "";
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex != 0;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {        
        if (((String) aValue).equals(""))
            JOptionPane.showMessageDialog(null, "Preenchimento obrigatório");
        else {
            switch(columnIndex){
            case 1:
                GeneroDAO generoDAO = new GeneroDAO();
                generoDAO.alterar(rowIndex, aValue);
                break;
            }
        fireTableCellUpdated(rowIndex, columnIndex);
        }
    }
}
