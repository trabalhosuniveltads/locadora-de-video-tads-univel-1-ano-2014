package modelo;

import DAO.Banco;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class ComboBoxModelLocacaoFilmes extends AbstractListModel implements ComboBoxModel{
    
    private String      selecionado = "";
    Banco banco = Banco.getInstance();
    
    // QUAL É A QUANTIDADE DE OPÇÕES NO COMBOBOX 
    @Override
    public int getSize() {
        return banco.getFilmes().size();
    }

    //QUAL O ELEMENTO A SER SELECIONADO
    @Override
    public Object getElementAt(int index) { 
        return banco.getFilmes().get(index).getTitulo();
    }

    //PEGA O ELEMENTO SELECIONADO E JOGA NA VARIAVEL
    @Override
    public void setSelectedItem(Object anItem) {
        selecionado = (String) anItem;
    }
    
    @Override
    public Object getSelectedItem() {
        return (String) selecionado;
    }
}
