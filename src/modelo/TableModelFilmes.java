package modelo;

import DAO.Banco;
import DAO.FilmeDAO;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import objeto.Filme;

public class TableModelFilmes extends AbstractTableModel{
            
    private List<Integer> ids = new ArrayList<>();    
    private boolean pesquisa;
    
    public void setPesquisa(List<Integer> ids, boolean pesquisa) {
        this.ids = ids;
        this.pesquisa = pesquisa;
    }
    
    @Override
    public int getRowCount() {
        return ids.isEmpty() && !pesquisa ? Banco.getInstance().getFilmes().size() : ids.size();
    }

    @Override
    public int getColumnCount() {
        return 7;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Banco b = Banco.getInstance();
        int row = ids.isEmpty() ? rowIndex : ids.get(rowIndex);
        Filme filme = b.getFilmes().get(Integer.valueOf(row));
        
        switch (columnIndex) {
            case 0:
                return row;
            case 1:
                return filme.getTitulo();
            case 2:
                return filme.getAnoLancamento();
            case 3:
                return filme.getQuantidade();
            case 4:
                return filme.getCensura();
            case 5:
                return filme.getGenero();
            case 6:
                if (filme.isLancamento())
                    return "SIM";
                else
                    return "NÃO";
            default:
                return "";
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "ID";
            case 1:
                return "Título";
            case 2:
                return "Ano Lançamento";
            case 3:
                return "Quantidade";
            case 4:
                return "Censura";
            case 5:
                return "Gênero";
            case 6:
                return "Lançamento";
            default:
                return "";
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex != 0;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        FilmeDAO filmeDAO = new FilmeDAO();
        
        if (((String) aValue).equals(""))
            JOptionPane.showMessageDialog(null, "Preenchimento obrigatório");
        else {
            switch(columnIndex){
            case 1:
                filmeDAO.alterar(columnIndex, rowIndex, aValue);
                break;
            case 2:
                if (((String)aValue).length() == 4) 
                    filmeDAO.alterar(columnIndex, rowIndex, Integer.valueOf((String)aValue));                   
                break;
            case 3:
                filmeDAO.alterar(columnIndex, rowIndex, Integer.valueOf((String)aValue));
                break;
            case 4:
                for (String censura : ComboBoxModelFilmesCensura.censura) 
                    if (((String) aValue).equals(censura)) {
                        filmeDAO.alterar(columnIndex, rowIndex, aValue);
                        break;
                    }
                break;
            case 5:
                Banco banco = Banco.getInstance();
                for (int i=0;i<banco.getGeneros().size();i++)
                    if (((String) aValue).equals(String.valueOf(banco.getGeneros().get(i)))) {
                        filmeDAO.alterar(columnIndex, rowIndex, aValue);
                        break;
                    }
                break;
            case 6:
                switch ((String)aValue) {
                    case "SIM":
                        filmeDAO.alterar(columnIndex, rowIndex, true);
                        break;
                    case "NÃO":
                        filmeDAO.alterar(columnIndex, rowIndex, false);
                        break;
                    default:
                        break;
                }
                break;
            }
        }
        fireTableCellUpdated(rowIndex, columnIndex);
    }
   
}
