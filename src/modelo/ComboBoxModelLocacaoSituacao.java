package modelo;

import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class ComboBoxModelLocacaoSituacao extends AbstractListModel implements ComboBoxModel{
    
    public static final String[]    situacao     = {"Ativo", "Cancelado", "Entregue", "Perdido"};
    private String      selecionado = "";
    
    // QUAL É A QUANTIDADE DE OPÇÕES NO COMBOBOX 
    @Override
    public int getSize() {
        return situacao.length;
    }

    //QUAL O ELEMENTO A SER SELECIONADO
    @Override
    public Object getElementAt(int index) { 
        return situacao[index];
    }

    //PEGA O ELEMENTO SELECIONADO E JOGA NA VARIAVEL
    @Override
    public void setSelectedItem(Object anItem) {
        selecionado = (String) anItem;
    }
    
    @Override
    public Object getSelectedItem() {
        return (String) selecionado;
    }
}
