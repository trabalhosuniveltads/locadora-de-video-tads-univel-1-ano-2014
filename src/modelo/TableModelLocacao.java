package modelo;

import DAO.Banco;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import objeto.Cliente;
import objeto.Locacao;

public class TableModelLocacao extends AbstractTableModel{

    Banco b = Banco.getInstance();
    private List<Integer> ids = new ArrayList<>();    
    private boolean pesquisa;
    
    public void setPesquisa(List<Integer> ids, boolean pesquisa) {
        this.ids = ids;
        this.pesquisa = pesquisa;
    }
    
    @Override
    public int getRowCount() {
        return ids.isEmpty() && !pesquisa ? Banco.getInstance().getLocacoes().size() : ids.size();
    }

    @Override
    public int getColumnCount() {
        return 6;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Banco b = Banco.getInstance();
        int row = ids.isEmpty() ? rowIndex : ids.get(rowIndex);
        Locacao locacao = b.getLocacoes().get(Integer.valueOf(row));
        switch (columnIndex) {
            case 0:
                return row;
            case 1:
                return locacao.getCliente().getNome();
            case 2:
                return locacao.getDataLocacao();
            case 3:
                return locacao.getSituacao();
            case 4:
                return locacao.getQtdItens();
            case 5:
                return (locacao.getValorTotalPago() > locacao.getValorTotal())  ? locacao.getValorTotalPago() : locacao.getValorTotal();
            default:
                return "";
        }
    }

    @Override
    public String getColumnName(int column) {
        switch (column) {
            case 0:
                return "Pedido N.";
            case 1:
                return "Cliente";
            case 2:
                return "Data Empréstimo";
            case 3:
                return "Situação";
            case 4:
                return "Quantidade Itens";
            case 5:
                return "Total";
            default:
                return "";
        }
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }
}
