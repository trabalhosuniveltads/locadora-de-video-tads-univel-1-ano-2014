package modelo;

import DAO.Banco;
import javax.swing.AbstractListModel;
import javax.swing.ComboBoxModel;

public class ComboBoxModelLocacaoCliente extends AbstractListModel implements ComboBoxModel{
    
    private String      selecionado = "";
    Banco banco = Banco.getInstance();
    
    // QUAL É A QUANTIDADE DE OPÇÕES NO COMBOBOX 
    @Override
    public int getSize() {
        return banco.getClientes().size();
    }

    //QUAL O ELEMENTO A SER SELECIONADO
    @Override
    public Object getElementAt(int index) { 
        return index + " - " + banco.getClientes().get(index).getNome();
    }

    //PEGA O ELEMENTO SELECIONADO E JOGA NA VARIAVEL
    @Override
    public void setSelectedItem(Object anItem) {
        selecionado = (String) anItem;
    }
    
    @Override
    public Object getSelectedItem() {
        return (String) selecionado;
    }
}
