package objeto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

public class Locacao implements Serializable{

    public Cliente                        cliente;
    private HashMap<Integer, LocacaoItem> itens;
    private Date                          dataLocacao;
    private String                        situacao;
    private float                         valorTotal;
    private float                         valorTotalPago;
    private int                           qtdItens;

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public HashMap<Integer, LocacaoItem> getItens() {
        return itens;
    }

    public void setItens(HashMap<Integer, LocacaoItem> itens) {
        this.itens = itens;
    }

    public Date getDataLocacao() {
        return dataLocacao;
    }

    public void setDataLocacao(Date dataLocacao) {
        this.dataLocacao = dataLocacao;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public float getValorTotal() {
        return valorTotal;
    }

    public float getValorMulta() {
        //melhorar
        return valorTotalPago - valorTotal;
    }
    
    public void setValorTotal(float valorTotal) {
        this.valorTotal = valorTotal;
    }

    public float getValorTotalPago() {
        return valorTotalPago;
    }

    public void setValorTotalPago(float valorTotalPago) {
        this.valorTotalPago = valorTotalPago;
    }

    public int getQtdItens() {
        return qtdItens;
    }

    public void setQtdItens(int qtdItens) {
        this.qtdItens = qtdItens;
    }
    
    
}
