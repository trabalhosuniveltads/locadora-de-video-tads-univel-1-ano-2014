package objeto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

public class LocacaoItem implements Serializable{

    private Filme                   filme;
    private Date                    dataEmprestimo;
    private Date                    dataDevolucao;
    private int                     atrazo;
    private String                  situacao;
    private float                   valorTotal;
    private float                   valorTotalPago;

    public LocacaoItem(Filme filme, Date dataEmprestimo, Date dataDevolucao, int atrazo, String situacao, float valorTotal, float valorTotalPago) {
        this.filme = filme;
        this.dataEmprestimo = dataEmprestimo;
        this.dataDevolucao = dataDevolucao;
        this.atrazo = atrazo;
        this.situacao = situacao;
        this.valorTotal = valorTotal;
        this.valorTotalPago = valorTotalPago;
    }

    public Filme getFilme() {
        return filme;
    }

    public void setFilme(Filme filme) {
        this.filme = filme;
    }


    public Date getDataEmprestimo() {
        return dataEmprestimo;
    }

    public void setDataEmprestimo(Date dataEmprestimo) {
        this.dataEmprestimo = dataEmprestimo;
    }

    public Date getDataDevolucao() {
        return dataDevolucao;
    }

    public void setDataDevolucao(Date dataDevolucao) {
        this.dataDevolucao = dataDevolucao;
    }

    public int getAtrazo() {
        return atrazo;
    }

    public void setAtrazo(int atrazo) {
        this.atrazo = atrazo;
    }

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public float getValorTotal() {
        return valorTotal;
    }

    public void setValorTotal(float valorTotal) {
        this.valorTotal = valorTotal;
    }

    public float getValorTotalPago() {
        return valorTotalPago;
    }

    public void setValorTotalPago(float valorTotalPago) {
        this.valorTotalPago = valorTotalPago;
    }

}
