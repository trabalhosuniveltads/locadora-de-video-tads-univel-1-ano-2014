package objeto;

import java.io.Serializable;
import java.math.BigDecimal;

public class Locadora implements Serializable {

    private String     nome;
    private String     telefone;
    private double     valorDiariaFilme;
    private int        diasLocacaoFilme;
    private double     valorDiariaFilmeLancamento;
    private int        diasLocacaoFilmeLancamento;
    private double     valorMultaDia;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public double getValorDiariaFilme() {
        return valorDiariaFilme;
    }

    public void setValorDiariaFilme(double valorDiariaFilme) {
        this.valorDiariaFilme = valorDiariaFilme;
    }

    public int getDiasLocacaoFilme() {
        return diasLocacaoFilme;
    }

    public void setDiasLocacaoFilme(int diasLocacaoFilme) {
        this.diasLocacaoFilme = diasLocacaoFilme;
    }

    public double getValorDiariaFilmeLancamento() {
        return valorDiariaFilmeLancamento;
    }

    public void setValorDiariaFilmeLancamento(double valorDiariaFilmeLancamento) {
        this.valorDiariaFilmeLancamento = valorDiariaFilmeLancamento;
    }

    public int getDiasLocacaoFilmeLancamento() {
        return diasLocacaoFilmeLancamento;
    }

    public void setDiasLocacaoFilmeLancamento(int diasLocacaoFilmeLancamento) {
        this.diasLocacaoFilmeLancamento = diasLocacaoFilmeLancamento;
    }

    public double getValorMultaDia() {
        return valorMultaDia;
    }

    public void setValorMultaDia(double valorMultaDia) {
        this.valorMultaDia = valorMultaDia;
    }

}
